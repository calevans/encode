#!/bin/bash
#
# process.sh
# copyright 2022 - Cal Evans <cal@calevans.com>
#
# This is the main entry point to run vote process.
#
# It takes 2  parameters:
# $1=process (process, graphic, publish)
# $2= episode number
docker run -it -v /mnt/data/vote:/opt/vote vote:latest app/process.php $1 -e $2 -v
