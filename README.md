# Podcast Tools

This series of command line programs is designed to do the bulk of the processing that I have to do for my podcast, Voices of the ElePHPant. There are currently 3 pieces

1. **encode** - stitches together the pieces of the audio as well as ads and uploads the final toS3 for use.
1. **graphics** - Creates the social media graphics that are embedded in the html. Currently only produces the twitter card, soo, Facebook.
1. **publish** - Publish an episode to the website in draft mode.

## To use

1. Clone this project locally
1. Run `composer install`
1. Rename `config/config.sample` to `config/config.php`.
1. Make sure that `config/config.php` has all the directories listed in `config.php` are created.
1. Add your AWS credentials to `config/config.php`.

### Encoding audio

```
$ ./process.php encode
```

This will read the `raw/` directory and encode and upload all episodes present.

```
$ ./process.php encode-e XXX
```

This will encode a single episode, XXX and upload it to S3.

## id3 files

In the id3 directory, you need to place a text file for each episode with the id3 tag info in it.

The name of the file has to be `<EPISODE_NUM>.id3`. Inside it place the following information.

```
artist=John McAfee
album=Voices of the ElePHPant
title=Interview with John McAfee
episode=4021
genre=Speech
publisher=E.I.C.C., Inc.
composer=Cal Evans
year=2034
image=logo.jpg
twitterHandle=officialmcafee
```

The `image` file name is assumed to be inside the graphics directory. Do not put directory info in the id3 file.

### Creating Graphics
To create the social media graphics, use the **graphics** command

```
$ ./process.php graphics
```

or

```
$ ./process.php graphics -e 299
```

The first one will process all the episodes that have **ID3** files, the second one will attempt to proces the episode number specified, assuming it has an **ID3** file.

This will create all formats known and upload them to the proper directories on S3 for use.

This command will make use of

```
twitterHandle=officialmcafee
```
in the **ID3** fie if it exists. For twitter, if the guest's image does not already exist, it will check to see if they have a twitter account and if so, use that image.

# Version history

 - 1.0.0 Initial release
 - 2.0.0 Refactor
 - 2.1.0 `graphics` now generating a TwitterCard
 - 2.1.1 Refactor
 - 3.0.0 `publish` now working