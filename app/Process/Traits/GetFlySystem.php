<?php
namespace Process\Traits;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp;

trait GetFlySystem {
  protected function getFlySystem(
    array $storage
  ) : Filesystem {

    switch ($storage['primary'])
        {
          case 'ftp':
            $adapter = new Ftp(
              [
                'host' => $storage['ftp']['host'], // required
                'username' => $storage['ftp']['login'], // required
                'password' => $storage['ftp']['password'], // required
                'port' => 21,
                'ssl' => false,
                'timeout' => 90,
                'utf8' => false,
                'passive' => true,
                'transferMode' => FTP_BINARY,
                'systemType' => null, // 'windows' or 'unix'
                'ignorePassiveAddress' => null, // true or false
                'timestampsOnUnixListingsEnabled' => false, // true or false
                'recurseManually' => true // true
              ]
            );

            break;
          case 'aws' :
            $client = new S3Client([
              'credentials' => [
                'key'    => $storage['aws']['key'],
                'secret' => $storage['aws']['secret'],
              ],
              'region' => $storage['aws']['region'],
              'version' => 'latest',
            ]);
          $adapter = new AwsS3Adapter($client, $storage['aws']['bucket']);
          break;

        }

    return new Filesystem($adapter);
  }

}