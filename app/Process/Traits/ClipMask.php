<?php

namespace Process\Traits;

trait ClipMask {

  public function clipMask(\Imagick $guestsPicture, bool $drawBorder = true): \Imagick
  {
    $width = $guestsPicture->getImageWidth();
    $height = $guestsPicture->getImageHeight();

    // Step 1 Create the overall mask
    $clipMask = new \Imagick();
    $clipMask->newPseudoImage(
      $width,
      $height,
      "canvas:transparent"
    );

    // Step 2 create the circle mask
    $circleMask = new \ImagickDraw();
    $circleMask->setFillColor('white');
    $circleMask->circle(($width / 2), ($height / 2), 0, ($width / 2));
    $clipMask->drawImage($circleMask);

    // Step 3 Lay the mask on top of the picture.
    $guestsPicture->compositeimage(
      $clipMask,
      \Imagick::COMPOSITE_COPYOPACITY,
      0,
      0
    );

    // OPTIONAL Step 4 Lay the border on top
    if ($drawBorder) {
      $border = new \ImagickDraw();
      $border->setStrokeColor('white');
      $border->setStrokeWidth(5);
      $border->setStrokeOpacity(1);
      $border->setFillOpacity(0);
      $border->circle(($width / 2), ($height / 2), 3, ($height / 2));
      $guestsPicture->drawImage($border);
    }

    return $guestsPicture;
  }
}