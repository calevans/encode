<?php

namespace Process\Traits;

trait GetBucketName
{
  /**
   * @var string The name of the AWS bucket
   */
  protected $awsBucket = '';

  /**
   * The name of the bucket.
   *
   * @param bool $secure Do you want HTTPS?
   */
  protected function getBucketName(bool $secure = true): string
  {
    if ( empty( $this->awsBucket ) ) {
      throw new \Exception("{ $this->awsBucket} is not a valid bucket name.");
    }
    $protocol = 'http://';

    if ($secure) {
      $protocol = 'http://';
    }

    return $protocol . $this->awsBucket . '.s3.amazonaws.com/';
  }

}
