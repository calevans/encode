<?php

namespace Process\Traits;

trait GetPath {
  /**
   * Return the proper path.
   *
   * @return string
   */
  protected $paths = [];

  protected function getPath(string $path, bool $raw = false) : string {
    if (! isset($this->paths[$path])) {
      throw new \Exception("{$path} is not a valid path.");
    }

    if ($raw) {
      return $this->paths[$path];
    }

    return $this->paths['root'] . $this->paths[$path];
  }

}