<?php

namespace Process\Traits;

trait GenerateFileName
{
  /**
   * The sprintf format string for the audio file name
   *
   * @var string
   */
  protected $fileNamePattern = '';

  /**
   * Uses the pattern passed in to convert the episode number to a file name.
   * An extension can optionally be passed in as well.
   *
   * @param int $episode The episode number to be applied
   * @param string $extension The file extension to be applied. The Default
   *                           is mp3.
   *
   * @return string
   * @throws \Exception if no pattern is passed set.
   */
  protected function generateFileName(int $episode, string $extension = 'mp3', string $nameAppend = ''): string
  {
    if (empty($this->fileNamePattern)) {
      throw new \Exception("{ $this->fileNamePattern} is not a valid pattern.");
    }

    return sprintf($this->fileNamePattern,$episode)
      . (!empty($nameAppend) ? ('_' . $nameAppend) : '')
      . '.'
      . $extension;
  }
}
