<?php

namespace Process\Traits;

trait FetchEpisodeList {

  protected function fetchEpisodesList(string $path, int $singleEpisode) : array
  {
    $episodes = [];

    foreach( glob( $path . '*.mp3' ) as $file ) {
      $nameParts = explode('_',basename($file));

      // Wrong format
      if (! isset($nameParts[2])) {
        continue;
      }

      if (! in_array($nameParts[1], $episodes) ) {
        $episodes[] = $nameParts[1];
      }
    }

    ksort($episodes);

    if ($singleEpisode !== 0 ) {
    $episodes = array_filter(
        $episodes,
        function($value) use($singleEpisode) {
        return (int)$value === (int)$singleEpisode;
        });
    }

    if (count($episodes) === 0 ) {
      throw new \Exception('No episodes to process.');
    }

    return $episodes;
  }

}