<?php

namespace Process\GraphicGenerators;

use Process\Model\EpisodeConfig;
use Symfony\Component\Console\Output\OutputInterface;

interface GraphicGeneratorInterface {
  public function __construct(
    EpisodeConfig $episodeConfig,
    array $config,
    OutputInterface $output
  );

  public function build() : \Imagick;

}