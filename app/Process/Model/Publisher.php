<?php

namespace Process\Model;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\OutputInterface;

use Process\Traits\GetPath;
use Process\Traits\GetBucketName;
use Process\Traits\GenerateFileName;
use Twig\Loader\FilesystemLoader;
use GuzzleHttp\Client;
use JamesHeinrich\GetID3\GetID3;
use Process\GraphicGenerators\TwitterCard;
use Process\GraphicGenerators\FacebookCard;
use Process\PublisherAssistant\PublisherAssistant;
use Twig\Environment;

/**
 * Class representation of the id3 file and all extra data contained.
 *
 * @todo Right now twitterCard and FacebookCard are hard coded. Need to
 *       abstract the 'generate list of generators' so they can be
 *       dynamically used. All we do with them is use the TYPE constant.
 */
class Publisher
{
  use GetPath;
  use GetBucketName;
  use GenerateFileName;

  protected $episodeConfig    = null;
  protected $templateFileName = '';
  protected $apiCredentials   = [];
  protected $rendered         = '';
  protected $pictureObject    = null;
  protected $debug            = false;
  protected $enclosure        = '';
  protected $application      = null;
  protected $output           = null;
  protected $builder  = null;
  protected $config  = [];

  /**
   * Create a new Publisher object
   *
   * @param Application $application
   * @param EpisodeConfig $episodeConfig
   * @param array $apiCredentials Different from what you can get form the
   *              application object.
   * @param OutputInterface $output
   */
  public function __construct(
    Application $application,
    EpisodeConfig $episodeConfig,
    array $apiCredentials,
    OutputInterface $output,
    array $config
    ) {
    $this->application      = $application;
    $this->paths            = $this->application->config['paths']; // have to have this for getPath to work.
    $this->episodeConfig    = $episodeConfig;
    $this->templateFileName = $this->application->config['template']['fileName'];
    $this->apiCredentials   = $apiCredentials;
    $this->awsBucket        = $this->application->config['storage']['aws']['bucket'];
    $this->fileNamePattern  = $this->application->config[ 'fileNamePattern'];
    $this->output           = $output;
    $this->debug            = $this->application->config['debug'];
    $this->builder = new PublisherAssistant($this);
    $this->config = $config;
  }

  /**
   * Main function This is called by the command object
   */
  public function build() : void {
    $this->checkConstraints()
      ->buildEnclosure()
      ->buildTemplate()
      ->pushToWeb();
  }

  /**
   * Builds the enclosure that will be inserted into the WordPress post
   *
   * @return Publisher
   */
  protected function buildEnclosure() : Publisher {
    $getID3 = new GetID3();
    $baseAudioFileName = $this->generateFileName($this->episodeConfig->episode,'mp3');
    $fileInfo = $getID3->analyze(
      $this->paths['root'] . $this->paths['finished'] . $baseAudioFileName
    );
    // $this->enclosure = $this->getBucketName() . $baseAudioFileName . "\n";
    $this->enclosure = $this->config['storage']['ftp']['base_url'] .
      $this->getPath('episodes', true) .
      $this->generateFileName($this->episodeConfig->episode) . "\n";

    $this->enclosure .=  $fileInfo['filesize'] . "\n";
    $this->enclosure .=  $fileInfo['mime_type'] . "\n";
    $this->enclosure .=  serialize(['duration' => gmdate('H:i:s', $fileInfo['playtime_seconds'])]);

    return $this;
  }

  /**
   * Called to check to make sure we have everything we need to execute.
   *
   * @return Publisher
   * @throws \Exception
   */
  protected function checkConstraints() : Publisher {

    if (!file_exists($this->getPath('template') . $this->templateFileName)) {
      throw new \Exception('Template ' . $this->getPath('template') . $this->templateFileName . 'does not exist.');
    }

    return $this;
  }

  /**
   * Instantiate twig, render it with the data from the EpisodeConfig
   *
   * @todo have to upload the picture to either the site or S3 before we can use it.
   */
  protected function buildTemplate() : Publisher {
    $loader = new FilesystemLoader($this->getPath('template')); // pass in
    $twig   = new Environment($loader); // pass in

    $template = $twig->load($this->templateFileName);
    $this->rendered = $this->builder->build($template);

    return $this;
  }


  /**
   * Call the WordPress REST API endpoint to either create or update the post.
   *
   */
  protected function pushToWeb() : Publisher {
    $client = new Client(); // pass in

    $response = $client->request(
      'GET',
      $this->apiCredentials['base_uri'] .
        $this->apiCredentials['episode_uri'] . '/' . $this->episodeConfig->episode,
      [
        'headers' => [
          'Accept'     => 'application/json',
          'Authorization' => $this->getAuthorization()
        ],
        'debug' => $this->debug,
        'http_errors' => false
      ]
    );

    // a lot of this is hard coded and we need to move it to variables.

    $payload = [
      'post_author' => 1, // don't hard code this!
      'post_content' => $this->rendered,
      'post_title' => strtr( $this->episodeConfig->title, '|',' '),
      'comment_status' => 'closed',
      'ping_status' => 'closed',
      'post_category' => 'podcast',
      'tags_input' => $this->episodeConfig->artist,
      'meta_input' => [
        'episode_id' => $this->episodeConfig->episode,
        'artist' => $this->episodeConfig->artist,
        'album' => $this->episodeConfig->album,
        'title' => strtr( $this->episodeConfig->title, '|',' '),
        'genre' => $this->episodeConfig->genre,
        'year' => $this->episodeConfig->year,
        'composer' => $this->episodeConfig->composer,
        'twitterHandle' => $this->episodeConfig->twitterHandle,
        'twitter_card_url' => $this->builder->getTwitterCardUrl(),
        'audioUrl' => $this->config['storage']['ftp']['base_url'] . $this->getPath('episodes', true) . $this->generateFileName($this->episodeConfig->episode,'mp3'),
        'facebook_graphic_url' => $this->builder->getFacebookCardUrl(),
        'enclosure' => $this->enclosure,
        'retired' => $this->episodeConfig->retired
      ]
    ];

    $this->output->writeln(
      "Episode {$this->episodeConfig->episode} "
      . ( $response->getStatusCode() === 200 ? 'FOUND' : 'NOT FOUND' ),
      OutputInterface::VERBOSITY_DEBUG
    );

    switch ($response->getStatusCode()) {

      case 200:
        $client = new Client();
        $response = $client->request(
          'PATCH',
          $this->apiCredentials['base_uri']
            . $this->apiCredentials['episode_uri']
            . '/'
            . $this->episodeConfig->episode,
          [
            'json' => $payload,
            'headers' => [
              'Authorization' => $this->getAuthorization(),
              'Accept'        => 'application/json',
              'Content-type'  => 'application/json',
            ],
            'debug' => $this->debug
          ]
        );
        break;

      case 404:
        $client = new Client();
        $response = $client->request(
          'POST',
          $this->apiCredentials['base_uri']
            . $this->apiCredentials['episode_uri'],
          [
            'json' => $payload,
            'headers' => [
              'Authorization' => $this->getAuthorization(),
              'Accept'        => 'application/json',
              'Content-type'  => 'application/json',
            ],
            'debug' => $this->debug
          ]
        );
        break;

      default:
        throw new \Exception(
          'Received status code ' . $response->getStatusCode()
        );
    }

    return $this;
  }

  public function getApiCredentials() : array {
    return $this->apiCredentials;
  }

  public function getApplication() {
    return $this->application;
  }

  public function getEpisodeConfig() {
    return $this->episodeConfig;
  }

  public function getAuthorization() : string
  {
    return 'Basic ' . base64_encode($this->config['website_api']['username'] .':'. $this->config['website_api']['password']);
  }
}
