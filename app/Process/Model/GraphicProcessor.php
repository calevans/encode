<?php

namespace Process\Model;

use League\Flysystem\Filesystem;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\OutputInterface;
use ZendService\Twitter\Twitter;
use \Imagick;

use Process\Traits\ {
  GetPath,
  GenerateFileName
};

use Process\Model\EpisodeConfig;
use Process\GraphicGenerators\AbstractGraphicGenerator;

/**
 * Graphic Processor
 * This class marshals all of the individual graphic processors generators the
 * graphics and saves them.
 *
 * It will:
 * 1: Get a list of Graphic Processor classes from ./GraphicGenerators
 * 2: Instantiate them one at a time.
 * 3: Call each one as it is instantiated, take it's output of an ImagMagic
 *    class, and save it to the proper place using Flysystem.
 *
 * Assumptions
 * - Generators will have a constant 'rootPath' that is the place inside the
 *   AWS bucket that the files will be saved
 * - config.php will have a section named the same as the graphic generator
 *   class that will contain all of it's config info.
 *
 */
class GraphicProcessor
{
  use GetPath;
  use GenerateFileName;

  protected $episodeConfig = null;
  protected $processors    = [];
  protected $flysystem     = null;
  protected $typeToBuild   = 'all';
  protected $config        = [];
  protected $output        = null;
  protected $application   = null;

  public function __construct(
    Application $application,
    EpisodeConfig $episodeConfig,
    Filesystem $flysystem,
    string $typeToBuild,
    OutputInterface $output
  ) {
    $this->application     = $application;
    $this->output          = $output;
    $this->config          = $application->config;
    $this->paths           = $application->config['paths'];
    $this->episodeConfig   = $episodeConfig;
    $this->flysystem       = $flysystem;
    $this->typeToBuild     = $typeToBuild;
    $this->fileNamePattern = $application->config['fileNamePattern'];
  }

  public function build() {
    $rawGraphicName = $this->getPath('pictures') .
      $this->episodeConfig->episode . '.jpg';

    if ( ! file_exists($rawGraphicName) ) {
      $this->fetchImageFromTwitter();
    }

    $this->fetchGeneratorList();

    foreach ( $this->processors as $generatorType=>$generator) {
      $this->output->writeln(
        "Generating : " . substr($generator, strrpos($generator, '\\') + 1),
        OutputInterface::VERBOSITY_DEBUG
      );
      $imageGenerator = new $generator(
        $this->episodeConfig,
        $this->config,
        $this->output
      );
      $image = $imageGenerator->build();
      $this->upload($image, $imageGenerator);
    }
  }

  protected function fetchGeneratorList()  {

    foreach (glob( $this->getPath('graphicGenerators') . '/*.php') as $file) {

      $type = $this->getType($file);
      if (empty($type)) {
        continue;
      }

      if ($this->typeToBuild === 'ALL') {
        $this->processors[$type[0]] = $type[1];
        continue;
      }
      if ( $type[0] !== $this->typeToBuild ) {
        continue;
      }

      $this->processors[ $type[0]] = $type[1];
    }
  }

  protected function getType(string $file) : array {

    if (strpos($file,'Interface') !== false ) {
      return [];
    }
    // This is a kludge. it needs to go.
    $command = "php -r 'include(\"" .$this->config["paths"]["root"] ."vendor/autoload.php\");include(\"{$file}\");\$x=get_declared_classes();echo \$x[count(\$x)-1];'";

    $class = shell_exec($command);

    return [ $class::TYPE,$class];
  }

  /**
   * Upload the file to S3
   *
   * @return $this
   * @throws \Exception
   */
  protected function upload(Imagick $image, $generator) {
    $outputFileName = $this->config['storage']['ftp']['base_dir'] . $this->getPath('graphics',true) .
    $this->generateFileName($this->episodeConfig->episode,'jpg',$generator::TYPE);

    $response = $this->flysystem->put(
      $outputFileName,
      $image,
      ['visibility' => 'public']
    );

    if ( !$response ) {
      throw new \Exception("Failed to upload {$outputFileName}");
    }
  }

  /**
   * If we don't have a graphic for this episode, see if they have a twitter
   * account. If they do, pull the graphic from that.
   */
  protected function fetchImageFromTwitter() : void {

    $rawGraphicName = $this->getPath('pictures') .
      $this->episodeConfig->episode . '.jpg';

    if (
      empty($this->episodeConfig->twitterHandle) ||
        $this->episodeConfig->twitterHandle === 'not used'
    ) {
      return;
    }
    $twitter = new Twitter($this->application->config['twitter']);

    $response = $twitter->users->lookup($this->episodeConfig->twitterHandle);

    if (is_array($response->errors)) {
      throw new \Exception(
        "Trying to fetch image from Twitter returned the following error\n"
        . print_r($response->errors,true)
      );
    }

    $imageUrl = $response->toValue()[0]->profile_image_url_https;
    $imageUrl = preg_replace('/(.*)_normal(.*)/','${1}_400x400${2}',$imageUrl);
    file_put_contents($rawGraphicName,file_get_contents($imageUrl));
  }

}