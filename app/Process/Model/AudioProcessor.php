<?php
namespace Process\Model;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\OutputInterface;

use JamesHeinrich\GetID3\GetID3;
use JamesHeinrich\GetID3\WriteTags;

use League\Flysystem\Filesystem;

use Process\Traits\GetPath;
use Process\Traits\GenerateFileName;
use Process\Model\EpisodeConfig;

/**
 * MP3 Processor
 * This class will process a single podcast episode.
 *
 * It will:
 * 1: Find all the pieces of a given episode
 * 2: Determine how many-if any-ad slots there are
 * 3: Organize the pieces together and put them in order.
 * 4: Stich the pieces together
 * 5: Build an id3 tag based on the id3 file
 * 6: Upload the finished file to an AWS S3 bucket
 *
 * Assumptions
 * - Podcast episodes are named in this EXACT format
 *   TOKEN_EPISODENO_PARTNO.mp3
 *   e.g.  vote_297_1.mp3
 *         vote_297_2.mp3
 *         vote_297_2.mp3
 * - If a file named pre_roll.mp3 exists, it will be prepended to the episode.
 * - If a file named post_roll.mp3 exists, it will be appended to the episode.
 * - If files named ad_slot_X.mp3 exist, they will be inserted between sections.
 * - If a section exists and there is no ad_slot file, the next segment will be appended.
 *
 * @todo logo is hard coded for a jpeg right now. needs to accept more types.
 */
class AudioProcessor
{
  use GetPath;
  use GenerateFileName;

  protected $episode          = 0;
  protected $episodePieces    = [];
  protected $outputFileName   = '';
  protected $paths            = [];
  protected $outputFilesystem = null;
  protected $application      = null;
  protected $output           = null;
  protected $config           = [];

  public function __construct (
    Application $application,
    Filesystem $outputFilesystem,
    EpisodeConfig $episodeConfig,
    OutputInterface $output,
    array $config
  ) {
    $this->application      = $application;
    $this->paths            = $this->application->config['paths']; // have to have this for getPath to work.
    $this->outputFilesystem = $outputFilesystem;
    $this->episodeConfig    = $episodeConfig;
    $this->fileNamePattern  = $this->application->config['fileNamePattern']; // have to have this for generateFileName to work
    $this->output           = $output;
    $this->config           = $config;
  }

  /**
   * Main processing function. Call this, it will either work or throw an
   * exception.
   */
  public function build() {
    $this->gatherEpisodeFiles()
         ->computeFinalName()
         ->integrateAds()
         ->concatEpisodePieces()
         ->addId3Tag()
         ->upload();
  }

  /**
   * Compute the name of the finished file
   */
  protected function computeFinalName() {
    if (empty($this->episodePieces)) {
      throw new \Exception(
        'Cannot computer final file name because there are no episode pieces ' .
        'for episode #' . $this->episodeConfig->episode
      );
    }

    $this->outputFileName = $this->getPath('finished') .
      $this->generateFileName($this->episodeConfig->episode);

    $this->output->writeln(
      "Output File Name:",
      OutputInterface::VERBOSITY_DEBUG
    );
      $this->output->writeln(
        "   {$this->outputFileName}",
        OutputInterface::VERBOSITY_DEBUG
      );
    return $this;
  }

  /**
   * Build a list of the episode pieces for the given episode.
   *
   * @todo This needs to use the file name template instead of depending on
   *       positional.
   */
  protected function gatherEpisodeFiles() {
    $thisEpisode = $this->episodeConfig->episode;

    $this->episodePieces = array_filter(
      glob($this->getpath('raw') . '*.mp3'),
      function($value) use ($thisEpisode) {
        return (int)explode('_',$value)[1] === (int)$thisEpisode;
      }
    );

    sort($this->episodePieces);
    return $this;
  }

  /**
   * Integrate the appropriate ads into the episodePieces array
   *
   * @todo the body of this needs to be a loop, not dependant on the idea that
   *       there are only 2 or 3 pieces.
   */
  protected function integrateAds() {
    if (empty($this->episodePieces)) {
      throw new \Exception('No episode pieces for episode #' . $this->episodeConfig->episode);
    }

    $finalEpisodePieces = [];

    // if we have a pre-roll, use it
    if (file_exists($this->getPath('ads') . 'pre_roll.mp3')) {
      $finalEpisodePieces[]= $this->getPath('ads') . 'pre_roll.mp3';
    }

    $finalEpisodePieces[] = $this->episodePieces[0];

    // If we have a part _2 then put an ad between them.
    if (isset($this->episodePieces[1])) {
      $finalEpisodePieces[] =  $this->getPath('ads') . 'ad_slot_1.mp3';
      $finalEpisodePieces[] =  $this->episodePieces[1];
    }

    // if we have a part _3 AND we have an ad_slot_2 stitch them in
    if (isset($this->episodePieces[2]) ) {
      if (file_exists($this->getPath('ads') . 'ad_slot_2.mp3')) {
        $finalEpisodePieces[] =  $this->getPath('ads') . 'ad_slot_2.mp3';
      }
      $finalEpisodePieces[] =  $this->episodePieces[2];
    }

    // if we have a post-roll, use it
    if (file_exists($this->getPath('ads') . 'post_roll.mp3')) {
      $finalEpisodePieces[]= $this->getPath('ads') . 'post_roll.mp3';
    }

    $this->episodePieces = $finalEpisodePieces;

    if ($this->output->isDebug()) {
      $this->output->writeln(
        "Episode Pieces:",
        OutputInterface::VERBOSITY_DEBUG
      );

      foreach ($this->episodePieces as $piecesParts) {
        $this->output->writeln(
          "  {$piecesParts}",
          OutputInterface::VERBOSITY_DEBUG
        );
      }

    }


    return $this;
  }

  /**
   * Concatenate all the pieces together into a single MP3.
   *
   * we use sox to do this. Just concatenating MP3 files screws with the
   * timing and I've never found a way to correctly.
   *
   * I would love a PHP based solution for this.
   */
  protected function concatEpisodePieces() {
    if (empty($this->episodePieces)) {
      throw new \Exception(
        "No episode pieces for episode # {$this->episodeConfig->episode}"
      );
    }


    if ( file_exists($this->outputFileName)) {
      unlink($this->outputFileName);
    }

    $command = 'sox --clobber --combine concatenate ';

    foreach($this->episodePieces as $piece) {
      $command .= $piece . ' ';
    }

    $command .= $this->outputFileName;
    $this->output->writeln(
      "sox command:",
      OutputInterface::VERBOSITY_DEBUG
    );
    $this->output->writeln(
      "  {$command}",
      OutputInterface::VERBOSITY_DEBUG
    );

    exec($command);

    return $this;
  }

  /**
   * Write the id3 tag to the file
   */
  protected function addId3Tag() {
    $getID3 = new GetID3();
    $getID3->setOption(array('encoding'=>'UTF-8'));

    $tagwriter = new WriteTags();
    $tagwriter->filename = $this->outputFileName;
    $tagwriter->tagformats = array('id3v2.3');

    $tagwriter->overwrite_tags    = true;
    $tagwriter->tag_encoding      = 'UTF-8';
    $tagwriter->remove_other_tags = true;


    $tagData = [
      'title'            => [$this->episodeConfig->title],
      'artist'           => [$this->episodeConfig->artist],
      'album'            => [$this->episodeConfig->album],
      'year'             => [$this->episodeConfig->year],
      'genre'            => [$this->episodeConfig->genre],
      'composer'         => [$this->episodeConfig->composer],
      'track'            => [$this->episodeConfig->episode],
      'publisher'        => [$this->episodeConfig->publisher],
      'attached_picture' => []
    ];
    $tagData['attached_picture'][] = [
        'data'          => file_get_contents(
          $this->getPath('graphics') . $this->episodeConfig->image
        ),
        'picturetypeid' => 0x03, // front cover
        'description'   => 'logo',
				'mime'          => 'image/jpeg'
    ];

    $tagwriter->tag_data = $tagData;

    if (! $tagwriter->WriteTags()) {
      throw new \Exception(
        'Failed to write tags!<br>' .
        implode("\n\n", $tagwriter->errors)
      );
    }

    return $this;
  }

  /**
   * Upload the file to S3
   */
  protected function upload()
  {
    $fileHandle = fopen($this->outputFileName,'r');
    $outputFileName = $this->config['storage']['ftp']['base_dir'] .
      $this->getPath('episodes', true) .
      $this->generateFileName($this->episodeConfig->episode);

    $response = $this->outputFilesystem->putStream(
      $outputFileName,
      $fileHandle,
      ['visibility' => 'public']
    );

    fClose($fileHandle);

    if (! $response) {
      throw new \Exception("Failed to upload {$this->outputFileName}");
    }

    return $this;
  }

}