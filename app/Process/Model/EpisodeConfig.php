<?php

namespace Process\Model;

use Process\Traits\GetPath;

/**
 * Class representation of the id3 file and all extra data contained.
 *
 * @todo implement a schema so we aren't exploding EVERYTHING with a pipe in it.
 */
class EpisodeConfig
{

  use GetPath;

  protected $data = [];
  protected $episodeNumber = -1;

  public function __construct(int $episodeNumber, array $paths) {
    $this->paths = $paths;
    $this->episodeNumber = $episodeNumber;
    $this->readFile($this->getPath('config') . "sponsor.txt");
    $this->readFile($this->getPath('id3') . "{$this->episodeNumber}.id3");
  }

  protected function readFile(string $fileName) {
    if (
      ! file_exists($fileName)
      ) {
        throw new \Exception(
          "Config file {$fileName} does not exist."
        );
    }

    foreach( file($fileName) as $item) {
      $item  = trim($item);
      if (empty($item)) {
        continue;
      }
      $key   = substr($item,0,strpos($item,'='));
      $value = substr($item,strpos($item,'=')+1);

      if ($key === 'showNotes') {
        if (strpos($value,'|') !== false) {
          $holding = explode('|',$value);
          $value = $holding;
        }
        $this->data[$key][] = $value;
        continue;
      }

      $this->data[$key] = $value;
    }
  }

  public function __get($key) {
    if ($key === 'episode') {
      return $this->episodeNumber;
    }

    if (! isset($this->data[$key])) {
      throw new \Exception("Episode {$this->episodeNumber} does not contain {$key}.");
    }

    return $this->data[$key];
  }

  public function __isset($key) {
    return isset($this->data[$key]);
  }

  public function toArray(): array
  {
    return $this->data;
  }
}