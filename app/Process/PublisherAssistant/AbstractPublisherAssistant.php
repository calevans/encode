<?php

namespace Process\PublisherAssistant;

use Twig\Environment;

abstract class AbstractPublisherAssistant {
  abstract public function build( Environment $template) : string;
  abstract public function getTwitterCardUrl() : string;
  abstract public function getFacebookCardUrl() : string;
}