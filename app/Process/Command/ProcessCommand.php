<?PHP
namespace Process\Command;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

use Process\Model\AudioProcessor;
use Process\Model\EpisodeConfig;
use Process\Traits\FetchEpisodeList;
use Process\Traits\GetPath;
use Process\Traits\GetFlySystem;


class ProcessCommand extends Command
{
  use FetchEpisodeList;
  use GetPath;
  use GetFlySystem;

  protected $testing = false;
  public $output  = null;

  protected function configure()
  {
      $definition = [
        new InputOption(
          'episode',
          'e',
          InputOption::VALUE_REQUIRED,
          'Only process a single episode'
        )
      ];

      $this->setName('process')
            ->setDescription('Compile all MP3 files')
            ->setDefinition($definition)
            ->setHelp(
              'Compile all of the episodes of of a podcast located in the '
              . 'RAW directory with the ads located in ads.'
            );
      return;
  }

  /**
   * Main call for the symfony command console
   */
  protected function execute(InputInterface $input, OutputInterface $output) : int
  {
    $this->output = $output;
    $this->paths  = $this->getApplication()->config['paths'];

    $this->displayHeader();

    $singleEpisode = (int)$input->getOption('episode') ?? 0;
    $episodes      = $this->fetchEpisodesList($this->getPath('raw'), $singleEpisode);

    foreach($episodes as $episode) {
      $this->output->writeln(
        "Processing {$episode}",
        OutputInterface::VERBOSITY_VERBOSE
      );

      (new AudioProcessor(
        $this->getApplication(),
        $this->getFlySystem(
          $this->getApplication()->config['storage']
        ),
        new EpisodeConfig(
          (int)$episode,
          $this->getApplication()->config['paths']
        ),
        $this->output,
        $this->getApplication()->config
      ))->build();
    }

    $this->displayFooter();

    return 0;
  }

  protected function displayHeader() : void
  {
      $class_name = (new \ReflectionClass($this))->getShortName();

      $this->output->writeln(" ", OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln("Process - " . $class_name, OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln("  By: Cal Evans <cal@calevans.com>", OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln(" ", OutputInterface::VERBOSITY_NORMAL);

      return;
  }

  protected function displayFooter(?string $errorMessage = '') : void {
      $this->output->writeln(" ",OutputInterface::VERBOSITY_NORMAL);

      if (strlen($errorMessage)>0) {
          $this->output->writeln("Error : ",OutputInterface::VERBOSITY_NORMAL);
          $this->output->writeln($errorMessage,OutputInterface::VERBOSITY_NORMAL);
          $this->output->writeln("",OutputInterface::VERBOSITY_NORMAL);
      }

      $this->output->writeln("Done");

      return;
  }

}
