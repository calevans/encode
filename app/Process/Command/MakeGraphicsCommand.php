<?PHP
namespace Process\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

use Process\Model\EpisodeConfig;
use Process\Traits\FetchEpisodeList;
use Process\Traits\GetPath;
use Process\Traits\GetFlySystem;
use Process\Model\GraphicProcessor;

class MakeGraphicsCommand extends Command
{
  use FetchEpisodeList;
  use GetPath;
  use GetFlySystem;

  protected function configure()
  {
    $definition = [
      new InputOption('episode', 'e', InputOption::VALUE_REQUIRED, 'Only process a single episode'),
      new InputOption('type', 't', InputOption::VALUE_REQUIRED, 'Only process a single type of graphic')
    ];

    $this->setName('graphics')
            ->setDescription('Make social media graphics for episodes')
            ->setDefinition($definition)
            ->setHelp('Create social cards to be used for each episode or the single episode specified.');
    return;
  }

  protected function execute(InputInterface $input, OutputInterface $output) :int
  {
    $this->output  = $output;
    $this->paths = $this->getApplication()->config['paths'];

    $this->displayHeader();

    $singleEpisode = (int)$input->getOption('episode') ?? 0;
    $typeToBuild = $input->getOption('type') ?? 'ALL';

    $episodes = $this->fetchEpisodesList($this->getPath('raw'), $singleEpisode);

    foreach($episodes as $episode) {
      $this->output->writeln(
          "  Processing {$episode}",
          OutputInterface::VERBOSITY_VERBOSE
      );
      (new GraphicProcessor(
        $this->getApplication(),
          new EpisodeConfig(
            $episode,
            $this->getApplication()->config['paths']
          ),
        $this->getFlySystem(
          $this->getApplication()->config['storage']
        ), // don't need to do this here.
        $typeToBuild,
        $this->output
      ))->build();

    }

    $this->displayFooter();

    return 0;
  }

  protected function displayHeader() : void {
      $class_name = (new \ReflectionClass($this))->getShortName();

      $this->output->writeln(" ", OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln("Process - " . $class_name, OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln("  By: Cal Evans <cal@calevans.com>", OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln(" ", OutputInterface::VERBOSITY_NORMAL);

      return;
  }

  protected function displayFooter(?string $errorMessage = '') : void {
      $this->output->writeln(" ",OutputInterface::VERBOSITY_NORMAL);

      if (strlen($errorMessage)>0) {
          $this->output->writeln("Error : ",OutputInterface::VERBOSITY_NORMAL);
          $this->output->writeln($errorMessage,OutputInterface::VERBOSITY_NORMAL);
          $this->output->writeln("",OutputInterface::VERBOSITY_NORMAL);
      }

      $this->output->writeln("Done");

      return;
  }

}
