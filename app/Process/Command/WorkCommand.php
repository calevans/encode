<?PHP
namespace Process\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use GuzzleHttp\Client;
use Process\Model\EpisodeConfig;


class WorkCommand extends Command
{

  /** @var string $token The JWT token */
  protected $token         = null;

  protected function configure()
  {
    $definition = [];

    $this->setName('work')
         ->setDescription('work')
         ->setDefinition($definition)
         ->setHelp('Work');

    return;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output  = $output;
    $this->debug = $this->output->isDebug();
    $episode=322;
    $this->apiCredentials = $this->getApplication()->config['website_api'];
    // update the vocabulary file
    // upload the vocab file to S3
    // Call updateVocabulary
    // delete the file from S3

    // stitch together the episode without ads
    /*
     * This will require a re-write of ProcessCommand. All the actual work needs to be done in Process.php
     * Process.php needs to take as a parameter $raw true orr false. If true then do not insert ads. default is false.
     */
    // upload the mp3
    // transcribe
    // save the transcription in transcriptions/episode.txt
  }

}
