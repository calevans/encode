<?PHP
namespace Process\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

use Process\Traits\FetchEpisodeList;
use Process\Traits\GetPath;
use Process\Model\Publisher;
use Process\Model\EpisodeConfig;

use \GuzzleHttp\Client;

/**
 * Publish a single episode to the WordPress backend
 */
class PublishCommand extends Command
{
  use FetchEpisodeList;
  use GetPath;

  /** @var string $token The JWT token */
  protected $token         = null;

  /**
   * Configure this command for the application
   */
  protected function configure() : void {
    $definition = [
        new InputOption('episode', 'e', InputOption::VALUE_REQUIRED, 'Only publish a single episode')
    ];

    $this->setName('publish')
            ->setDescription('Publish an episode to the web')
            ->setDefinition($definition)
            ->setHelp('Add a new episode to the website or if the episode already exists, update it.');
  }

  /**
   * Entry point for all Symfony Commands
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  protected function execute(InputInterface $input, OutputInterface $output) : int
  {
    $this->output  = $output;
    $this->paths   = $this->getApplication()->config['paths'];
    $singleEpisode = (int)$input->getOption('episode')??0;
    $this->getApplication()->config['debug'] = $this->output->isDebug();

    $this->displayHeader();
    // $this->getToken();
    $this->mainLoop($singleEpisode);
    $this->displayFooter();

    return 0;
  }


  /**
   * Main loop
   *
   * @param int $singleEPisode an episode id or 0 for all.
   */
  protected function mainLoop(int $singleEpisode) : void {
    $episodes = $this->fetchEpisodesList($this->getPath('raw'), $singleEpisode);
    foreach($episodes as $episode) {
      $this->output->writeln(
        "  Processing {$episode}",
        OutputInterface::VERBOSITY_VERBOSE);

      // $apiCredentials['token'] = $this->token;

      (new Publisher (
        $this->getApplication(),
        (new EpisodeConfig(
            $episode,
            $this->getApplication()->config['paths']
          )
        ),
        $this->getApplication()->config['website_api'],
        $this->output,
        $this->getApplication()->config
        )
      )->build();
    }
  }

  /**
   * Fetch the token from the WordPress instance
   *
   * Use the username and password to get the JWT.
   */
  protected function getToken() : void {
    $client = new Client();
    $response = $client->request(
      'POST',
      $this->getApplication()->config['website_api']['base_uri'] . $this->getApplication()->config['website_api']['token_uri'],
      [
        'form_params' => [
          'username' => $this->getApplication()->config['website_api']['username'],
          'password' => $this->getApplication()->config['website_api']['password']
        ],
        'headers' => [
            'Accept'     => 'application/json',
        ]
      ]
    );

      $this->token = json_decode($response->getBody(), true);

    // not sure I'll use this.
    $response = $client->request(
      'GET',
      $this->getApplication()->config['website_api']['base_uri'] . 'wp/v2/users/?search=' . $this->token['user_email'],
      ['headers' => [
          'Accept'              => 'application/json',
          'Authorization'       => 'Bearer ' . $this->token['token']
      ],
      'debug'=> false
      ]
    );

    $this->token['user'] = @json_decode($response->getBody()->getContents(),true)[0];
  }

  /**
   * Display the program's header
   */
  protected function displayHeader() : void {
    $class_name = (new \ReflectionClass($this))->getShortName();

    $this->output->writeln(" ", OutputInterface::VERBOSITY_NORMAL);
    $this->output->writeln("Publish - " . $class_name, OutputInterface::VERBOSITY_NORMAL);
    $this->output->writeln("  By: Cal Evans <cal@calevans.com>", OutputInterface::VERBOSITY_NORMAL);
    $this->output->writeln(" ", OutputInterface::VERBOSITY_NORMAL);
  }

  /**
   * Display the program's footer and any error messages passed in
   *
   * @param string $erorMessage Optional error message
   */
  protected function displayFooter(?string $errorMessage = '') : void {
    $this->output->writeln(" ",OutputInterface::VERBOSITY_NORMAL);

    if (strlen($errorMessage)>0) {
      $this->output->writeln("Error : ",OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln($errorMessage,OutputInterface::VERBOSITY_NORMAL);
      $this->output->writeln("",OutputInterface::VERBOSITY_NORMAL);
    }
    $this->output->writeln("Done");
  }

}
