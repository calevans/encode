#!/usr/bin/env php
<?php
require_once __DIR__.'/../vendor/autoload.php';

use Process\Command\ProcessCommand;
use Process\Command\MakeGraphicsCommand;
use Process\Command\PublishCommand;
use Process\Command\WorkCommand;

use Symfony\Component\Console\Application;

$app = new Application('Process', '2.3.0');

// decide which config to use
$config = include realpath(dirname(__FILE__) . '/../') . "/config/config.php";
$app->config = $config;

$app->addCommands(
  [
    new ProcessCommand(),
    new MakeGraphicsCommand(),
    new PublishCommand(),
    new WorkCommand(),
    ]
  );

$app->run();
